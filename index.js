// variable
let fullName = "Alfian Zulfikar";
let age = 24;
let isMarried = false;

// array
let hobbies = ["Ngoding", "Nonton film", "Nonton bola"];

// percabangan
if (isMarried) {
  console.log("Sudah nikah");
} else {
  console.log("Belum nikah");
}

// perulangan
let hobbyItem = "";
for (let i = 0; i < hobbies.length; i++) {
  hobbyItem = `${i + 1}. ${hobbies[i]}`;
  console.log(hobbyItem);
}

// prosedur
const addAge = (currentAge) => {
  age = currentAge + 1;
};

// function
const countHobby = (hobbies) => {
  return hobbies.length;
};

// algoritma pencarian (binary search)
const binarySearch = (arr, x) => {
  let start = 0;
  let end = arr.length - 1;

  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    if (arr[mid] === x) {
      return true;
    } else if (arr[mid] < x) {
      start = mid + 1;
    } else {
      end = mid - 1;
    }
  }

  return false;
};

// algoritma sorting (insertion sort)
const insertionSort = (inputArr) => {
  let newArr = inputArr;
  for (let i = 1; i < newArr.length; i++) {
    let key = newArr[i];
    let j = i - 1;
    while (j >= 0 && newArr[j] > key) {
      newArr[j + 1] = newArr[j];
      j = j - 1;
    }
    newArr[j + 1] = key;
  }

  return newArr;
};

// testing

// testing prosedur
addAge(age);
console.log("umur ditambah 1:", age);

// testing fungsi
console.log("jumlah hobi:", countHobby(hobbies));

// testing fungsi binarySearch
let arr = [1, 3, 5, 7, 8, 9];
let x = 10;

if (binarySearch(arr, x)) {
  console.log("Hasil pencarian: item ditemukan");
} else {
  console.log("Hasil pencarian: item tidak ditemukan");
}

x = 5;

if (binarySearch(arr, x)) {
  console.log("Hasil pencarian: item ditemukan");
} else {
  console.log("Hasil pencarian: item tidak ditemukan");
}

// testing fungsi insertionSort
let oldArr = [1, 4, 2, 6, 5, 4, 8, 1];
console.log("hasil sorting:", insertionSort(oldArr));
